const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const Product = require("../models/Product");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
})

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/archive", (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId/activate", auth.verify, (req, res) => {
	productController.archiveProduct(req.params, req.body).then( resultFromController => res.send(resultFromController));
})

module.exports = router;