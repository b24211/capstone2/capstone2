const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else {
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

module.exports.createOrder = (data) => {

	let newOrder = new Order({
		userId : data.userId,
		totalAmount : data.totalAmount
	})

	for( let i=0; i < data.products.length; i++){
	newOrder.products.push({productId : data.products[i].id}),
	newOrder.products.push({quantity : data.products[i].quantity})

	}

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

module.exports.getUserDetails = (data) => {
	return User.findById(data.userId).then(result => {

			result.password = "";

			return result;

		});

}

module.exports.getAllOrders = (data) => {
	if(data){
		return Order.find({}).then(result => {
			return result;

		})
	}
	else{
		return false;

	}
}

module.exports.getMyOrders = (data) => {
	if(data.isAdmin){
		return false;
	}
	else {
	return Order.find({userId : data.userId}).then(result => {
		return result;
	})
}
}

module.exports.setAsAdmin = (data) => {
	if(data.isAdmin){
		let updateAdmin = {
			isAdmin : true
		}

		return User.findByIdAndUpdate(data.userId, updateAdmin).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}
