const Product = require("../models/Product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {return result})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams, data) => {
	if(data.isAdmin){
	let archivedProduct = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((course,error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
	}
	else {
		return false;
	}
}